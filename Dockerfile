FROM atdr.meo.ws/archiveteam/mercurial-grab@sha256:c25841efe3679952eef9418a9784b09b7668918f753227890a10d3e3c404b108
COPY mercurial-dl /grab/
ENTRYPOINT ["./mercurial-dl"]
