A small wrapper around the mercurial-grab project container that dumps individual repositories on demand.

    docker build -t mercurial-dl:latest .
    docker run --rm -v $(pwd)/data:/grab/data mercurial-dl:latest https://hg.mozilla.org/penelope/

Accepts any number of URLs as arguments. The output WARCs are *not* merged together. See <https://archive.org/details/hg.libsdl.org_clone_warc_20210211> for an example of what the output might look like (although the directory naming here would be slightly different).
